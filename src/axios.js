import axios from "axios";

/* to make request to tmdb*/
const instance = axios.create({
    baseURL: "http://localhost:5001/fir-demo-ae05c/us-central1/api",
});

export default instance;
