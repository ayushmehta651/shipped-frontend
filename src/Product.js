import React from 'react'
import "./Product.css"
import { useStateValue } from './StateProvider';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Product({ id, title, image, price, rating }) {

    const [{ basket }, dispatch] = useStateValue();

    console.log("Basket >>>", basket);

    const addToBasket = () => {

        //dispatch some action into data layer
        dispatch({
            type: "ADD_TO_BASKET",
            item: {
                id: id,
                title: title,
                image: image,
                price: price,
                rating: rating,
            },
        });
        { toast("✅ Item Added To Cart") };
    }


    return (
        <div className="product">
            <div className="product__info">
                <p>{title}</p>
                <p className="product__price">
                    <small>₹</small>
                    <strong>{price}</strong>
                </p>
                <div className="product__rating">
                    {Array(rating).fill().map(() => (
                        <p>⭐️</p>
                    ))}
                </div>
            </div>
            <img className="product__image" src={image} alt="book_image" />
            <button onClick={addToBasket} className="product__button" >Add To Basket</button>
            {/*When ever u click "Add to baset" button it will push the product into 
               data layer -> React context API / REDUX
               And then we can pull it from Redux to any component we want ie.to checkout component
               
               Reducer :- How we are pulling and pushing data from data layer
            */}
        </div>
    )
}

export default Product;
