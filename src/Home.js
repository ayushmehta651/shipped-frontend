import React from 'react'
import "./Home.css"
import Product from './Product.js'
// import { Carousel } from 'react-bootstrap';
import HeroSlider, {
    Slide,
    Nav,
    SideNav,
    MenuNav,
    ButtonsNav,
    AutoplayButton,
    OverlayContent,
    OverlayContainer
} from 'hero-slider';

function Home() {
    return (
        <div className="home">
            <div className="home__container">
                <HeroSlider
                    slidingAnimation="left_to_right"
                    orientation="horizontal"
                    initialSlide={1}
                    style={{
                        // color: '#FFF'
                    }}
                    settings={{
                        slidingDuration: 500,
                        slidingDelay: 200,
                        shouldAutoplay: true,
                        shouldDisplayButtons: true,
                        autoplayDuration: 5000,
                        height: '80vmin',
                    }}
                >
                    <Slide
                        background={{
                            backgroundImage: 'https://images-eu.ssl-images-amazon.com/images/G/31/img20/Events/holi21/GW/rev/Holi-GW-Hero-PC-1x._CB658320183_.jpg',
                            // backgroundAttachment: 'fixed',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <Slide
                        background={{
                            backgroundImage: 'https://images-eu.ssl-images-amazon.com/images/G/31/img20/Sports/2021/hero/Sports_1500x600._CB656474946_.jpg',
                            // backgroundAttachment: 'fixed',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <Slide
                        background={{
                            backgroundImage: "https://images-eu.ssl-images-amazon.com/images/G/31/img21/Wireless/Apple/March/iPhone12/GW/V263044986_Wireless_iPhone_12_Launch_Tall_Hero_1500x600._CB656477505_.jpg",
                            // backgroundAttachment: 'fixed',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <Slide
                        background={{
                            backgroundImage: "https://images-eu.ssl-images-amazon.com/images/G/31/img21/Beauty/GW/March/Beauty-Herotators-PC-1500x600._CB656430787_.jpg",
                            // backgroundAttachment: 'fixed',
                            backgroundPosition: 'center center',
                        }}
                    />
                    <Nav />
                </HeroSlider>
                <div className="home__row">
                    <Product
                        id="12321341"
                        title="The Lean Startup: How Constant Innovation Creates Radically Successful Businesses Paperback"
                        price={1000}
                        rating={5}
                        image="https://images-na.ssl-images-amazon.com/images/I/51Zymoq7UnL._SX325_BO1,204,203,200_.jpg"
                    />
                    <Product
                        id="49538094"
                        title="Kenwood kMix Stand Mixer for Baking, Stylish Kitchen Mixer with K-beater, Dough Hook and Whisk, 5 Litre Glass Bowl"
                        price={20000}
                        rating={4}
                        image="https://images-na.ssl-images-amazon.com/images/I/81O%2BGNdkzKL._AC_SX450_.jpg"
                    />
                </div>

                <div className="home__row">
                    <Product
                        id="4903850"
                        title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor"
                        price={500}
                        rating={3}
                        image="https://images-na.ssl-images-amazon.com/images/I/71Swqqe7XAL._AC_SX466_.jpg"
                    />
                    <Product
                        id="23445930"
                        title="Amazon Echo (3rd generation) | Smart speaker with Alexa, Charcoal Fabric"
                        price={8000}
                        rating={5}
                        image="https://media.very.co.uk/i/very/P6LTG_SQ1_0000000071_CHARCOAL_SLf?$300x400_retinamobilex2$"
                    />
                    <Product
                        id="3254354345"
                        title="New Apple iPad Pro (12.9-inch, Wi-Fi, 128GB) - Silver (4th Generation)"
                        price={100}
                        rating={4}
                        image="https://images-na.ssl-images-amazon.com/images/I/816ctt5WV5L._AC_SX385_.jpg"
                    />
                </div>

                <div className="home__row">
                    <Product
                        id="90829332"
                        title="Samsung LC49RG90SSUXEN 49' Curved LED Gaming Monitor - Super Ultra Wide Dual WQHD 5120 x 1440"
                        price={99}
                        rating={4}
                        image="https://images-na.ssl-images-amazon.com/images/I/6125mFrzr6L._AC_SX355_.jpg"
                    />
                </div>
                <div className="home__row">
                    <Product
                        id="90829332"
                        title="Logitech G203 Prodigy Gaming Mouse – Black"
                        price={20000}
                        rating={5}
                        image="https://images-na.ssl-images-amazon.com/images/I/61d9C4yCB2L._SL1000_.jpg"
                    />
                    <Product
                        id="90829332"
                        title="Jenga Classic Game"
                        price={15000}
                        rating={5}
                        image="https://images-na.ssl-images-amazon.com/images/I/81OAWwX3djL._AC_SL1500_.jpg"
                    />
                    <Product
                        id="90829332"
                        title="Logitech Amazon Basics Nylon – Black"
                        price={200}
                        rating={5}
                        image="https://www.cellularline.com/medias/USBDATAC2LMFI60CMW-01-MAIN-HR.jpg?context=bWFzdGVyfHJvb3R8NDA5ODc5fGltYWdlL2pwZWd8aDcyL2g2Zi85MTAzOTQ3MjAyNTkwLmpwZ3wyNDc3NmE5MmIzMjUzZDViYTIyZDFmZDY4NTJiZjg3MjlkYjk3MzU0ZTlhNjFhMjgzOTRkMDhlOWEwZjEyZGJl"
                    />
                    <Product
                        id="90829332"
                        title="Amazon USB C Fast Charger -MFi Certified  – Black"
                        price={15000}
                        rating={5}
                        image="https://i01.appmifile.com/v1/MI_18455B3E4DA706226CF7535A58E875F0267/pms_1605851020.43266044.png"
                    />
                </div>
            </div>
        </div>
    )
}

export default Home
