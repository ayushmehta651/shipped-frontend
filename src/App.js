import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect } from 'react';
import "./App.css";
import { ToastContainer, toast } from 'react-toastify';
import Header from './Header';
import Checkout from './Checkout';
import Home from './Home';
import LogIn from './LogIn'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { auth } from './firebase';
import { useStateValue } from "./StateProvider";
import Payment from './Payment';
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import Orders from './Orders';

const promise = loadStripe(
  'pk_test_51IW3CZE0vIrZAQJ4UVT2SdtAInEi5w3pIk8BAniPayUu9v4eM1UufJVvsG4Jfdr1jtsOYKpgmStdeK4VYq2dRhFM00tVtTDaxy');

function App() {

  const [{ }, dispatch] = useStateValue()

  useEffect(() => {

    auth.onAuthStateChanged(authUser => {
      console.log(authUser)

      if (authUser) {

        dispatch({
          type: 'SET-USER',
          user: authUser
        })

      } else {

        dispatch({
          type: 'SET-USER',
          user: null

        })
      }
    })

  }, [])


  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path="/login">
            <LogIn />
          </Route>
          <Route path="/checkout">
            <Header />
            <Checkout />
          </Route>
          <Route path="/payment">
            <Header />
            <Elements stripe={promise}>
              <Payment />
            </Elements>
          </Route>
          <Route path="/orders">
            <Header />
            <Orders />
          </Route>
          <Route path="/">
            <Header />
            <Home />
          </Route>
        </Switch>
        <ToastContainer position="bottom-right" hideProgressBar={true} newestOnTop={true} closeOnClick />
      </div>
    </Router>
  )
}

export default App;
