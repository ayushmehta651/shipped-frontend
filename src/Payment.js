import React from 'react';
import { useStateValue } from './StateProvider';
import CheckoutProduct from "./CheckoutProduct";
import { Link } from "react-router-dom";
import "./Payment.css";
import axios from './axios';
import { useState, useEffect } from "react";
import CurrencyFormat from 'react-currency-format';
import { getBasketTotal } from './reducer';
import { useHistory } from "react-router-dom";
import { db } from './firebase.js';
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";

function Payment() {

    const stripe = useStripe();
    const elements = useElements();
    const history = useHistory();

    const [error, setError] = useState(null);
    const [disabled, setDisabled] = useState(true);
    const [succeeded, setSucceeded] = useState(false);
    const [processing, setProcessing] = useState("");
    const [clientSecret, setClientSecret] = useState(true);

    const [{ basket, user }, dispatch] = useStateValue();

    // useEffect(() => {
    //     //generate the special stripe secret which allows us to charge a customer
    //     const getClientSecret = async () => {
    //         const response = await axios({
    //             method: 'post',
    //             Stripe expects the total in a currencies subunits
    //             url: `/payments/create?total=${getBasketTotal(basket) * 100}`,
    //         });
    //         setClientSecret(response.data.clientSecret)
    //     }

    //     getClientSecret();

    // }, [basket])

    // console.log('THE SECRET IS >>>', clientSecret)
    // console.log('👱', user);

    const handleSubmitEvent = async (e) => {
        e.preventDefault();
        setProcessing(true);

        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: "card",
            card: elements.getElement(CardElement),
        });
        if (!error) {
            try {
                const { id } = paymentMethod;
                const response = await axios.post("http://localhost:3001/payment", {
                    amount: getBasketTotal(basket) * 100,
                    id,
                });

                if (response.data.success) {
                    console.log(response.data);
                    console.log("Successful payment");
                    setSucceeded(true);
                    setError(null);
                    setProcessing(false);

                    axios.post('http://localhost:3001/insert', {
                        basket: basket,
                        amount: response.data.amount,
                        createdAt: response.data.createdAt
                    })

                    dispatch({
                        type: 'EMPTY_BASKET'
                    })

                    history.push("/orders");
                }
            } catch (error) {
                console.log("Error", error);
            }
        } else {
            console.log(error.message);
        }
    }

    const handleChangeEvent = (e) => {
        //Listens for what the user types and 
        //shows error while typing
        setDisabled(e.empty);
        setError(e.error ? e.error.message : "");
    }

    return (
        <div className="payment">
            <div className="payment__container">

                <h1>
                    Checkout (
            <Link to="/checkout">{basket?.length} items</Link>
            )
            </h1>

                <div className="payment__section">
                    <div className="payment__title">
                        <h3>Delivery Address</h3>
                    </div>
                    <div className="payment__address">
                        <p>{user?.email}</p>
                        <p>H.NO:-851/1</p>
                        <p>Marathahalli</p>
                        <p>Bangalore</p>
                        <p>560037</p>
                    </div>
                </div>
                <div className="payment__section">
                    <div className="payment__title">
                        <h3>Review items</h3>
                    </div>
                    <div className="payment__items">
                        {basket.map(item => (
                            <CheckoutProduct
                                id={item.id}
                                title={item.title}
                                image={item.image}
                                price={item.price}
                                rating={item.rating}
                            />
                        ))}
                    </div>
                </div>
                <div className="payment__section">
                    <div className="payment__title">
                        <h3>Payment Method</h3>
                    </div>
                    <div className="payment__details">
                        <form onSubmit={handleSubmitEvent}>
                            <CardElement onChange={handleChangeEvent} />
                            <div className="payment__price">
                                <CurrencyFormat
                                    renderText={(value) => (
                                        <h3>Total : {value}</h3>
                                    )}
                                    decimalScale={2}
                                    value={getBasketTotal(basket)}
                                    displayType={"text"}
                                    thousandSeparator={true}
                                    prefix={"₹"}
                                />
                                <button disabled={processing || disabled || succeeded}>
                                    <span>{processing ? <p>Processing</p> : "Buy Now"}</span>
                                </button>
                            </div>
                            {error && <div>{error}</div>}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Payment
//firebase
                // db.collection('users')
                //     .doc(user?.uid)
                //     .collection('orders')
                //     .doc(paymentIntent.id)
                //     .set({
                //         basket: basket,
                //         amount: paymentIntent.amount,
                //         created: paymentIntent.created
                //     })