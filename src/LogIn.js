import React from 'react'
import './LogIn.css'
import { Link, useHistory } from "react-router-dom";
import { useState } from 'react';
import { auth } from "./firebase";

function LogIn() {
    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const SignIn = e => {
        e.preventDefault() //No refreshing

        auth.signInWithEmailAndPassword(email,password).then((auth) => {
            history.push("/")
        }).catch(error => (error.message))
    }

    const Register = e => {
        e.preventDefault();

        auth
            .createUserWithEmailAndPassword(email, password)
            .then((auth) => {
                // it successfully created a new user with email and password
                if (auth) {
                    history.push("/")
                }

                console.log(auth)
            })
            .catch(error => alert(error.message))
    }

    return (
        <div className="Login">
            <Link to="/">
                <img
                    className="Login__logo"
                    src="https://www.majorariatto.com/res/shipped/logo.png" alt="" />
            </Link>
            <div className="Login__container">
                <h1>Sign-in</h1>
                <form>
                    <h5>E-mail</h5>
                    <input type='text' value={email} onChange={e => setEmail(e.target.value)} />

                    <h5>Password</h5>
                    <input type='password' value={password} onChange={e => setPassword(e.target.value)} />

                    <button type='submit' onClick={SignIn} className='Login__signInButton'>Sign In</button>
                </form>
                <button
                    onClick={Register}
                    className="Login__registeButton">Register</button>
            </div>
        </div>
    )
}

export default LogIn
