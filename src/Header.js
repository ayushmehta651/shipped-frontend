import React from 'react';
import "./Header.css";
import SearchIcon from '@material-ui/icons/Search';
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import { Link } from "react-router-dom";
import { useStateValue } from './StateProvider';
import { auth } from './firebase';

function Header() {

    const [{ basket, user }, dispatch] = useStateValue();

    const handleAuthentication = () => {
        if (user) {
            auth.signOut()
        }
    }

    return (
        <div className="header">
            <Link to='/'>
                <img
                    className="header__logo"
                    src="https://www.majorariatto.com/res/shipped/logo.png" alt="logo" />
            </Link>

            <div className="header__search">
                <input
                    className="header_searchInput"
                    type="text" />
                <SearchIcon
                    className="header__searchIcon" />
            </div>

            <div className="header__nav">
                <Link to={!user && "/login"}>
                    <div onClick={handleAuthentication} className="header__option">
                        <div className="header__upper">
                            Hello
                    </div>
                        <div className="header_down">
                            {user ? 'Sign Out' : 'Sign In'}
                        </div>
                    </div>
                </Link>
                <Link to="/orders">
                    <div className="header__option">
                        <div className="header__upper">
                            Returns
                </div>
                        <div className="header_down">
                            & Orders
                </div>
                    </div>
                </Link>
                <div className="header__option">
                    <div className="header__upper">
                        Your
                </div>
                    <div className="header_down">
                        Prime
                </div>
                </div>

                <Link to='/checkout'>
                    <div className="header__optionBasket">
                        <ShoppingBasketIcon />
                        <div className="header__down header__basketCount">{basket?.length}</div>
                    </div>
                </Link>
            </div>
        </div>
    )
}

export default Header
