import firebase from "firebase"

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBAs_N9PrG1ooPfo64utLU2BB6qzo-Y9So",
    authDomain: "fir-demo-ae05c.firebaseapp.com",
    databaseURL: "https://fir-demo-ae05c.firebaseio.com",
    projectId: "fir-demo-ae05c",
    storageBucket: "fir-demo-ae05c.appspot.com",
    messagingSenderId: "164391586094",
    appId: "1:164391586094:web:70d8850a79e8f301b6d2bf",
    measurementId: "G-138EHPWNSJ"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };